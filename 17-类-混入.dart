// 类-继承和扩展
// 内容：
// 1. 继承
// 2. Mixin

void main() {
  Man man = Man('Jack', 20);
  man.say();
  man.drive();
  man.sing();

  Woman woman = Woman('Rose', 18);
  woman.say();
  woman.dressUp();
  woman.sing();
  woman.dance();

  Dancer dancer = Dancer('Tom', 40);
  dancer.say();
  dancer.dance();
}

class Person {
  Person(this.name, this.age);

  String? name;
  int? age;

  say() {
    print('我是$name今年$age岁');
  }
}

class Man extends Person with SingMixin {
  Man(super.name, super.age);

  // 擅长开车
  drive() {
    print('开车');
  }

  // sing() {
  //   print('唱歌');
  // }
}

class Woman extends Person with SingMixin, DanceMixin {
  Woman(super.name, super.age);

  // 擅长打扮
  dressUp() {
    print('打扮');
  }

  // sing() {
  //   print('唱歌');
  // }

  // dance() {
  //   print('跳舞');
  // }
}

class Dancer extends Person with DanceMixin {
  Dancer(super.name, super.age);

  // dance() {
  //   print('跳舞');
  // }
}

// 1. 角色 Dancer 舞蹈演员，擅长跳舞
// 2. 角色 Man 男人，擅长唱歌
// 3. 角色 Woman 女人，擅长跳舞+唱歌
mixin SingMixin {
  sing() {
    print('唱歌');
  }
}

mixin DanceMixin {
  dance() {
    print('跳舞');
  }
}
