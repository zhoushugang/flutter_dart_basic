// 数据类型-字典
// 内容：
// 1. 使用 Map 关键字修饰变量，语法 Map goods = { 'name': 'Mate60Pro', 'price': 6999, };
// 2. 查询 goods['name'];
// 3. 修改 goods['name'] = 'Mate70';
// 4. 新增 goods['count'] = 100;
// 5. 删除 goods.remove('count');
// 6. 遍历 forEach((key, value){});

void main() {
  // 1. 数据类型-字典定义
  Map goods = {
    'name': 'Mate60',
    'price': 6999,
  };
  print(goods);
  // 2. 查询 goods['name'];
  print(goods['name']);
  // 3. 修改 goods['name'] = 'Mate70';
  goods['name'] = 'Mate60Pro';
  print(goods);
  // 4. 新增 goods['count'] = 100;
  goods['count'] = 100;
  print(goods);
  // 5. 删除 goods.remove('count');
  goods.remove('count');
  print(goods);
  // 6. 遍历 forEach((key, value){});
  goods.forEach((key, value) {
    print('key $key, value $value');
  });
}
