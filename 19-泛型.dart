// 泛型
// 内容：
// 1. List 的泛型
// 2. Map 的泛型
// 3. 函数的泛型
// 4. Future 的泛型

// dynamic 动态类型

import 'dart:io';

void main() {
  // 1. List 的泛型
  List<int> list = [1, 2, 3, 4];
  List<String> categories = ['服饰', '电脑'];

  // 2. Map 的泛型
  // Map user = {
  //   'name': 'Jack',
  //   'age': 100,
  // };
  Map<String, String> goods = {
    'name': '电脑',
    'price': '100.00',
  };

  // 3. 函数泛型
  String str = getData<String>('jack');
  double num = getData<double>(10.0);

  // 4. Future 的泛型
  login().then((result) {
    print(result);
  });
}

// 需求：函数传入什么类型的数据就返回什么类型的数据
// String getString(String params) {
//   return params;
// }

// double getNum(double params) {
//   return params;
// }

T getData<T>(T params) {
  return params;
}

Future<String> login() {
  return Future<String>(() {
    sleep(Duration(seconds: 2));
    return 'token-10086';
  });
}
