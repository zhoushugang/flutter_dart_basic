// 流程控制：
// 内容：
// 1. 分支语句
// 2. 三元运算符
// 3. switch 语句
// 4. 循环 for  while  for in

void main() {
  // 1. 分支语句
  int score = 690;
  if (score > 700) {
    print('恭喜考上了清华');
  } else {
    print('继续努力');
  }
  // 2. 三元运算符
  print(score > 700 ? '恭喜考上了清华' : '继续努力');
  // 3. switch 语句
  // 1 待付款 2 待发货 3 待收货 4 待评价
  int state = 2;
  switch (state) {
    case 1:
      print('待付款');
      break;
    case 2:
      print('待发货');
      break;
    case 3:
      print('待收货');
      break;
    case 4:
      print('待评价');
      break;
    default:
      print('其他');
  }

  // 4. 循环 for  while  for in
  List categories = ['服饰', '美食', '数码'];
  for (var i = 1; i <= 10; i++) {
    print('第$i次');
  }
  for (var i = 0; i < categories.length; i++) {
    print(categories[i]);
  }

  int count = 1;
  while (count <= 10) {
    print('第$count次');
    count++;
  }

  for (var item in categories) {
    print(item);
  }
}
