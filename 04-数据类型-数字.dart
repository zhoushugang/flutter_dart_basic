// 数字类型-数字
// 内容：
// 1. num 关键字修饰变量，可整数或小数
// 2. int 关键字修饰变量，仅整数
// 3. double 关键字修饰变量，浮点数（支持赋值整数）

void main() {
  // 1. num 关键字
  num count = 10;
  count = 10.1;
  // print(count);
  // 2. int 关键字
  int num1 = 10;
  num1 = 11;
  // num1 = 11.1;

  // 3. double 关键字
  double price = 100.8;
  // price = 99.9;

  price = 100;

  print(price);
}
