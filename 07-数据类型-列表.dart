// 数据类型-列表
// 内容：
// 1. 使用 List 关键字修饰变量，语法 List list = [1, 2, 3];
// 2. 查询列表长度 length ，查询指定元素 list[索引]
// 3. 修改指定元素 list[索引] = 值;
// 4. 添加一个元素 add(元素)
// 5. 添加多个元素 addAll([元素, 元素])
// 6. 指定位置添加 list.insert(索引, 值);
// 7. 根据元素删除 remove(元素)
// 8. 根据索引删除 removeAt(索引)
// 9. 遍历 forEach((item){})

void main() {
  // 1. 定义列表
  List nums = [1, 2, 3, 4, 5];
  List categories = ['服饰', '美食', '数码'];
  print(nums);
  print(categories);
  // 2. 查询列表长度 length ，查询指定元素 list[索引]
  print(categories.length);
  print(categories[1]);
  // 3. 修改指定元素 list[索引] = 值;
  categories[1] = '家电';
  print(categories[1]);
  // 4. 添加一个元素 add(元素)
  categories.add('汽车');
  print(categories);
  // 5. 添加多个元素 addAll([元素, 元素])
  categories.addAll(['手机', '电脑']);
  print(categories);
  // 6. 指定位置添加 list.insert(索引, 值);
  categories.insert(1, '玩具');
  print(categories);
  // 7. 根据元素删除 remove(元素)
  categories.remove('玩具');
  print(categories);
  // 8. 根据索引删除 removeAt(索引)
  categories.removeAt(0);
  print(categories);
  // 9. 遍历 forEach((item){})
  categories.forEach((item) {
    print(item);
  });
}
