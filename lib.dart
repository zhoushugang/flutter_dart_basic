class Person {
  Person(this.name, this._age);

  String? name;
  // 私有数据，不能访问
  int? _age;

  say() {
    print('$name今年$_age岁');
  }
}
