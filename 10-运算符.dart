// 运算符
// 内容：
// 1. 算数运算 + - * / % ~/
// 2. 赋值运算 += -= *= /= %= ++ --，注意仅double类型可以 /=
// 3. 比较运算 > < >= <= == != ，全等判断相等
// 4. 逻辑运算 && || !

void main() {
  // 1. 算数运算
  double a = 100;
  double b = 3;

  print(a + b);
  print(a - b);
  print(a * b);
  print(a / b);
  print(a % b);
  // 出完取整
  print(a ~/ b);

  print('-----------');
  // 2. 赋值运算
  double count = 10;

  count += 10;
  print(count);
  count -= 10;
  print(count);
  count *= 10;
  print(count);
  // 只有 double 支持 /=
  count /= 10;
  print(count);

  count++;
  print(count);
  count--;
  print(count);

  print('-----------');
  // 3. 比较运算 > < >= <= == !=
  int x = 10;
  int y = 20;
  print(x > y);

  //  == !=
  print(false != 0);

  print('-----------');
  // 4. 逻辑运算
  int age = 36;
  int year = 15;
  // 年龄35以下，工龄10年
  print(age < 35 && year > 10);
  // 年龄35以下 或者 工龄12年
  print(age < 35 || year > 10);
  // 年龄35以下，工龄10年
  print(!(age < 35 && year > 10));

  print(!!false);
}
