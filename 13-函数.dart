// 函数
// 内容
// 1. 常规函数
// 2. 可选传参
// 3. 函数对象
// 4. 匿名函数
// 5. 箭头函数

void main() {
  // 1. 常规函数 + 参数 + 返回值
  say('Jack');
  String intro = getIntro('Tom', 18);
  print(intro);
  // 2. 可选传参
  String intro2 = getIntro('Jack', 18, gender: '男');
  print(intro2);
  // 3. 函数对象
  Function fn = say;
  fn('Tom');
  // 4. 匿名函数
  onClick(() {
    print('登录');
  });
  onClick(() {
    print('注册');
  });
  // 5. 箭头函数
  int sum = getSum(10, 40);
  print(sum);
}

void say(String name) {
  print('$name say Hello');
}

String getIntro(String name, int age, {String? gender}) {
  return '我是$name，今年$age岁，性别${gender != null ? gender : '保密'}';
}

void onClick(Function callback) {
  print('触发点击');
  callback();
}

// 箭头函数，函数体是一句话
// getSum(int a, int b) {
//   return a + b;
// }

int getSum(int a, int b) => a + b;
