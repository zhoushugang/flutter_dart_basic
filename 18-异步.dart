// 异步编程
// 内容：
// 1. 同步会阻塞程序运行
// 2. 使用 Future 异步处理
// 3. Future 链式调用
// 4. 使用 async await

import 'dart:io';

void main() {
  print('main 开始');
  // 1. 做一个耗时的操作
  // String data = getNetWorkData();
  // print(data);
  // 2. 通过异步执行
  // getNetWorkData().then((result) {
  //   print(result);
  // });
  // 3. 链式调用 异常处理
  // getNetWorkData().then((result) {
  //   print(result);
  // }).catchError((e) {
  //   print('错误原因：$e');
  // });
  // 4. async await 使用
  // login().then((token) {
  //   print(token);
  //   getUserInfo().then((userInfo) {
  //     print(userInfo);
  //   });
  // });
  doLogin();

  print('main 结束');
}

// 1. 同步执行会阻塞
// String getNetWorkData() {
//   sleep(Duration(seconds: 3));
//   return '网络数据';
// }

// 2. 使用 Future 异步处理
// getNetWorkData() {
//   return Future(() {
//     // 去进行耗时操作
//     sleep(Duration(seconds: 3));
//     return '网络数据';
//   });
// }

// 3. 链式调用，处理异常
// getNetWorkData() {
//   return Future(() {
//     // 去进行耗时操作
//     sleep(Duration(seconds: 3));
//     // return '网络数据';
//     throw Exception('网络异常');
//   });
// }

// 4. 使用 async await 优化异步操作
// 场景：登录，用户信息
login() {
  return Future(() {
    sleep(Duration(seconds: 2));
    return 'token-10086';
  });
}

getUserInfo() {
  return Future(() {
    sleep(Duration(seconds: 3));
    return 'Jack 18';
  });
}

doLogin() async {
  String token = await login();
  print(token);
  String userInfo = await getUserInfo();
  print(userInfo);
}
