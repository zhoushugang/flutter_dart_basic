// 类
// 内容：
// 1. 定义类，初始化
// 2. 默认构造函数-完整
// 3. 默认构造函数-简写
// 4. 命名构造函数
// 5. 私有属性方法
import './lib.dart';

void main() {
  // 1. 使用类
  // Person person = Person();
  // person.name = 'jack';
  // person.age = 18;
  // person.say();
  // 2. 使用类
  // Person person = Person('jack', 20);
  // person.say();

  // 3. 使用类
  // Person person = Person('jack', 20);
  // person.say();

  // 4. 使用类
  // Person person = Person('jack', 20);
  // person.say();
  // Person person2 = Person.init('tom', 18);
  // person2.say();

  // 5. 私有属性和方法
  Person person = Person('jack', 20);
  person.say();
  // print(person._age);
}

// 1. 定义类，初始化
// class Person {
//   String? name;
//   int? age;

//   say() {
//     print('$name今年$age岁');
//   }
// }

// 2. 默认构造函数-完整
// class Person {
//   Person(String name, int age) {
//     this.name = name;
//     this.age = age;
//   }

//   String? name;
//   int? age;

//   say() {
//     print('$name今年$age岁');
//   }
// }

// 3. 默认构造函数-简写
// class Person {
//   // Person(String name, int age) {
//   //   this.name = name;
//   //   this.age = age;
//   // }
//   Person(this.name, this.age);

//   String? name;
//   int? age;

//   say() {
//     print('$name今年$age岁');
//   }
// }

// 4. 命名构造函数
// class Person {
//   Person(this.name, this.age);
//   Person.init(this.name, this.age);

//   String? name;
//   int? age;

//   say() {
//     print('$name今年$age岁');
//   }
// }


// 5. 私有属性 lib.dart
