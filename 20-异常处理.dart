// 异常处理
// 内容
// 1. try 处理异常
// 2. await 异常处理

import 'dart:io';

void main() {
  // 1. try catch 用法
  // try {
  //   dynamic name = 'jack';
  //   name.haha();
  //   print(name.length);
  //   doLogin();
  // } catch (e) {
  //   print('处理异常：$e');
  // }

  doLogin();
}

doLogin() async {
  // 2. await 异常处理
  try {
    await getNetWorkData();
  } catch (e) {
    print('错误 $e');
  } finally {
    // 不过成功还是失败都执行
    print('finally');
  }
}

getNetWorkData() {
  return Future(() {
    // 去进行耗时操作
    sleep(Duration(seconds: 3));
    return '网络数据';
    // throw Exception('网络异常');
  });
}
