void main() {
  List carts = [
    {"count": 2, "price": 10.0, "selected": true},
    {"count": 1, "price": 30.0, "selected": false},
    {"count": 5, "price": 20.0, "selected": true}
  ];

  // 计算选中的商品总金额 forEach 或者 for
  double amount = 0;
  for (var i = 0; i < carts.length; i++) {
    Map cart = carts[i];
    if (cart['selected']) {
      amount += cart['price'] * cart['count'];
    }
  }
  print(amount);
}
