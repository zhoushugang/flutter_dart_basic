void main() {
  List carts = [
    {"count": 2, "price": 10.0, "selected": true},
    {"count": 1, "price": 30.0, "selected": false},
    {"count": 5, "price": 20.0, "selected": true}
  ];
  // 得到购物车是否全选
  bool isCheckAll = getCartCheckAll(carts);
  print(isCheckAll ? '全选' : '非全选');
}

bool getCartCheckAll(List carts) {
  bool isCheckAll = true;

  for (var i = 0; i < carts.length; i++) {
    Map cart = carts[i];
    if (!cart['selected']) {
      isCheckAll = false;
      break;
    }
    print(i);
  }
  return isCheckAll;
}
